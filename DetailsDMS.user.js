// ==UserScript==
// @name        DetailsDMS
// @namespace   https://dmscp.net/servers/*
// @description Gets server details to send out servers in DMSCP
// @downloadURL https://bitbucket.org/thisismetroid/detailsdms/raw/master/DetailsDMS.user.js
// @include     https://dmscp.net/servers/*
// @version     0.2.2
// @grant    	GM_addStyle

// ==/UserScript==

function createMessage() {
    var ip = document.getElementsByClassName("server-info")[1].getElementsByTagName('dd')[0].innerHTML;
    var ipmiIP = document.getElementsByClassName("server-info")[2].getElementsByTagName('dd')[0].innerHTML.match(/href="([^"]*)/)[1];
    var pass = document.getElementsByClassName("server-usage")[1].getElementsByTagName('dd')[1].innerHTML;
    var ipmiPass = document.getElementsByClassName("server-usage")[2].getElementsByTagName('dd')[1].innerHTML;
    var OS = document.getElementsByClassName("server-usage")[1].getElementsByTagName('dd')[0].innerHTML;
    var location = document.getElementsByClassName("server-info")[0].getElementsByTagName('dd')[2].innerHTML;
    var serverID = document.getElementsByClassName("server-usage")[0].getElementsByTagName('dd')[1].innerHTML;

    var messageFinal = "<pre>Server Details:\n" +
        "==========\n" +
        "Server Name: " + serverID.toString() + ".hosted-by-100tb.com\n" +
        "Main IP: " + ip.toString() + "\n" +
        "User: root / " + pass.toString() + "\n" +
        "Operating System: " + OS + "\n" +
        "Location: " + location.toString() + "\n" +
        "\n" +
        "IPMI Information:\n" +
        "==========\n" +
        "IP Address: " + ipmiIP.toString() + "\n" +
        "User: root / " + ipmiPass.toString() + "\n\n" + 
        "For pasting to spreadsheet (highlight cell(s) > data > split text into columns) \n==========\n" +
        serverID.toString() + "," + ip.toString() + "," + pass.toString() + "," + ipmiIP.toString() + " / " + ipmiPass.toString() + "," + location.toString() + "\n\n" +
        "For pasting to servers.lst(server check script)\n==========\n" +
        serverID.toString() + " " + ip.toString() + " " + pass.toString() + "</pre>";
	 console.log("message created");
    
    return messageFinal;
}

    function createMenuLink() {
        var manageMenu = document.getElementById("manage-server-menu");
        var provisionInfoLi = document.createElement("li");
        var link = document.createElement("a");
        link.setAttribute("id", "provLink");
        link.setAttribute("href", "javascript:;");
        link.innerHTML = "Provisioning Details";
        provisionInfoLi.appendChild(link);
        manageMenu.prepend(provisionInfoLi);
    }

    function createModal() {
        //Initialize Modal
        var modalOut = document.createElement("div");
        var modalIn = document.createElement("div");
        modalOut.setAttribute("id", "myModal");
        modalOut.setAttribute("class", "modal");
        modalIn.setAttribute("class", "modal-content");
        modalIn.innerHTML = createMessage();
        
        //Combine elements
        modalOut.appendChild(modalIn);
        document.body.appendChild(modalOut);
    }
    
    createMenuLink();
    createModal();

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("provLink");

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};

GM_addStyle("                                                    \
.modal {                                                         \
    display: none; /* Hidden by default */                       \
    position: fixed; /* Stay in place */                         \
    z-index: 1000; /* Sit on top */                              \
    padding-top: 100px; /* Location of the box */                \
    margin: 0 auto;                                              \
    width: 100%; /* Full width */                                \
    height: auto; /* Full height */                              \
    overflow: auto; /* Enable scroll if needed */                \
    background-color: rgb(0,0,0); /* Fallback color */           \
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */    \
}                                                                \
                                                                 \
/* Modal Content */                                              \
.modal-content {                                                 \
    background-color: #fefefe;                                   \
    margin: auto;                                                \
    overflow-x: auto;                                            \
    padding: 20px;                                               \
    border: 1px solid #888;                                      \
    width: 50%;                                                  \
}                                                                \
");
